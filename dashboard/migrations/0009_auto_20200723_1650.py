# Generated by Django 3.0.8 on 2020-07-23 13:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dashboard', '0008_auto_20200723_1640'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issue',
            name='trainer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='trainer', to=settings.AUTH_USER_MODEL),
        ),
    ]
