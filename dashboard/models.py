from django.contrib.auth.models import User
from django.db import models


class Issue(models.Model):
    PRIORITY_CHOICES = [
        (0, 'Information'),
        (1, 'Warning'),
        (2, 'Error'),
        (3, 'Critical'),
    ]

    STATUS_CHOICES = [
        (0, 'Pending'),
        (1, 'In progress'),
        (2, 'Suspend'),
        (3, 'Completed'),
    ]

    title = models.CharField(max_length=200, verbose_name="Краткое содержание")
    text = models.TextField(verbose_name="Текст")
    date_time = models.DateTimeField(auto_now_add=True, verbose_name="Дата и время")
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author')
    priority = models.IntegerField(choices=PRIORITY_CHOICES, blank=True, null=True, verbose_name="Приоритет")
    status = models.IntegerField(choices=STATUS_CHOICES, default=0, verbose_name="Статус")
    trainer = models.ForeignKey(User, on_delete=models.PROTECT, related_name='trainer', blank=True, null=True,
                                verbose_name="Тренер")

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-date_time',)


class Message(models.Model):
    text = models.TextField(help_text='Сообщение', verbose_name="Текст")
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now_add=True, verbose_name="Дата и время")
    issue = models.ForeignKey(Issue, on_delete=models.CASCADE)


    def __str__(self):
        return self.text


