from django.contrib import admin
from .models import Issue, Message


class MessageInline(admin.TabularInline):
    model = Message
    extra = 0


@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):
    list_display = ("title", 'author', 'date_time', 'priority', 'status')
    inlines = [MessageInline]

