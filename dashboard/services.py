from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from authentication.forms import StaffEditIssueForm, MessageForm, SearchTrainerForm
from dashboard.models import Issue, Message


def general_paginator(request, general_list):
    general_paginator = Paginator(general_list, 6)
    is_paginated = True if general_paginator.num_pages > 1 else False
    page_number = request.GET.get('page')
    page_obj = general_paginator.get_page(page_number)
    return page_obj, is_paginated


def dashboard_paginator(request):
    dashboard_list = get_issue_list(request)
    return general_paginator(request, dashboard_list)


def message_paginator(request, pk):
    message_list = Message.objects.filter(issue=pk)
    return general_paginator(request, message_list)


def get_issue_list(request):
    if request.user.is_staff:
        issue_list = Issue.objects.all()
    else:
        issue_list = Issue.objects.filter(author=request.user)
    return issue_list


def get_staff_form(post_data, issue):
    form = StaffEditIssueForm(instance=issue, data=post_data)

    if form.is_valid():
        form.save()

    return form


def get_message_form(request, issue):
    form = MessageForm()

    if request.method == 'POST':
        form = MessageForm(data=request.POST)
        if form.is_valid():
            message = form.save(commit=False)
            message.author = request.user
            message.issue = issue
            message.save()
            form = MessageForm()

    return form


def get_issue(pk):
    try:
        issue = Issue.objects.get(pk=pk)
    except ObjectDoesNotExist:
        return None

    return issue


def username_search_block(request):
    if request.method == "GET":
        username = request.GET.get('username')
        print(username)
        if username:
            users = User.objects.filter(username__contains=username)
            issue_list_username = Issue.objects.filter(author__in=users)
            return general_paginator(request, issue_list_username)
    else:
        return None


def trainer_search_form(request):
    issue_list_trainer = None
    if request.method == "GET":
        form_search = SearchTrainerForm(data=request.GET)
        data_form = form_search.data['trainer']
        if request.user.is_staff:
            if data_form:
                issue_list_trainer = Issue.objects.filter(trainer__in=data_form)
            else:
                issue_list_trainer = Issue.objects.filter(trainer=None)
        else:
            if data_form:
                issue_list_trainer = Issue.objects.filter(author=request.user).filter(trainer__in=data_form)
            else:
                issue_list_trainer = Issue.objects.filter(author=request.user).filter(trainer=None)
    return general_paginator(request, issue_list_trainer)
