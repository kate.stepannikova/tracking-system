from django.urls import path
from .views import dashboard, issue_page, ReportPage, search_user_for_staff, update_profile_page, success_message, \
     search_trainer_for_staff

urlpatterns = [
    path('', dashboard, name='dashboard'),
    path('issue/<int:pk>', issue_page, name='issue'),
    path('report/', ReportPage.as_view(), name='report'),
    path('search/', search_user_for_staff, name='search'),
    path('profile/', update_profile_page, name='profile'),
    path('profile/success/', success_message, name='success'),
    path('trainer-search/', search_trainer_for_staff, name='trainer_search'),
]