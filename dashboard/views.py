
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseForbidden, HttpResponseNotFound
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import FormView
from authentication.forms import SearchTrainerForm, ProfileForm, IssueForm
from .services import get_staff_form, get_message_form, get_issue, message_paginator, dashboard_paginator, \
    username_search_block, trainer_search_form


@login_required
def dashboard(request):
    page_obj, is_paginated = dashboard_paginator(request)
    form_search = SearchTrainerForm()
    context = {
        'page_obj': page_obj,
        'is_paginated': is_paginated,
        'form_search': form_search,
    }
    return render(request, 'dashboard/dashboard.html', context)

@login_required
def issue_page(request, pk):
    page_obj, is_paginated = message_paginator(request, pk)
    issue = get_issue(pk)
    form = get_message_form(request, issue)

    if request.user.is_staff:
        staff_form = get_staff_form(request.POST, issue)
    else:
        staff_form = None

    if not issue:
        return HttpResponseNotFound()

    if issue.author == request.user or request.user.is_staff:
        context = {
            "issue": issue,
            "form": form,
            "staff_form": staff_form,
            'page_obj': page_obj,
            'is_paginated': is_paginated,
        }
        return render(request, 'dashboard/issue.html', context)
    return HttpResponseForbidden()


class ReportPage(LoginRequiredMixin, FormView):
    form_class = IssueForm
    template_name = 'dashboard/report.html'
    success_url = reverse_lazy('dashboard')

    def form_valid(self, form):
        issue = form.save(commit=False)
        issue.author = self.request.user
        issue.save()

        return super().form_valid(form)


def search_user_for_staff(request):
    form_search = SearchTrainerForm()
    try:
        page_obj, is_paginated = username_search_block(request)
        context = {
            'page_obj': page_obj,
            'is_paginated': is_paginated,
            'form_search': form_search,
        }
    except:
        context = {'form_search': form_search}
    return render(request, 'dashboard/dashboard.html', context)


def search_trainer_for_staff(request):
    form_search = SearchTrainerForm()
    try:
        page_obj, is_paginated = trainer_search_form(request)
        context = {
            'page_obj': page_obj,
            'is_paginated': is_paginated,
            'form_search': form_search,
        }
    except TypeError:
        context = {'form_search': form_search}

    return render(request, 'dashboard/dashboard.html', context)


def update_profile_page(request):
    form = ProfileForm(instance=request.user)
    if request.method == 'POST':
        form = ProfileForm(data=request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('success')
    context = {
        'form': form
    }
    return render(request, 'dashboard/profile.html', context)


def success_message(request):
    return render(request, 'dashboard/Success_message.html')
