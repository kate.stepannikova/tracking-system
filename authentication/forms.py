from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from dashboard.models import Issue, Message


class UserForm(UserCreationForm):
    class Meta:
        model = User
        fields = '__all__'
        exclude = ('is_staff', 'is_active', 'date_joined',
                   'password', 'last_login', 'is_superuser',
                   'groups', 'user_permissions')

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

        for fieldname in ['username']:
            self.fields[fieldname].label = 'Логин'
        for fieldname in ['username', 'password1', 'password2']:
            self.fields[fieldname].help_text = None


class ProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', ]

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)

        for fieldname in ['username']:
            self.fields[fieldname].label = 'Логин'
            self.fields[fieldname].help_text = None


class IssueForm(forms.ModelForm):
    trainer = forms.ModelChoiceField(queryset=User.objects.filter(is_staff=True), required=False, label="Тренер")

    class Meta:
        model = Issue
        fields = '__all__'
        exclude = ('user', 'status', 'author')

class SearchTrainerForm(forms.ModelForm):
    trainer = forms.ModelChoiceField(queryset=User.objects.filter(is_staff=True), required=False, label="Тренер")

    class Meta:
        model = Issue
        fields = ('trainer',)



class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('text',)



class StaffEditIssueForm(forms.ModelForm):
    class Meta:
        model = Issue
        fields = ('priority', 'status')
