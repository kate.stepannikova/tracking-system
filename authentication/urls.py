from django.urls import path
from .views import login_page, register_page, redirect_to_login, logout_view

urlpatterns = [
    path('login/', login_page, name='login'),
    path('register/', register_page, name='register'),
    path('', redirect_to_login),
    path('logout/', logout_view, name='logout'),

]